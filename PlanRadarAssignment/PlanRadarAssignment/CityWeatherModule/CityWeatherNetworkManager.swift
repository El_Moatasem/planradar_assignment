//
//  CityWeatherNetworkManager.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 02/04/2022.
//

import Foundation
import Swinject

protocol CityWeatherNetworkManagerInterace: NetworkDataManagerInterface {
    func getWeatherForCity(city: CityObject, callback: @escaping ((DataResult<CityWeatherInfoObject>) -> Void))
}

class CityWeatherNetworkManager: CityWeatherNetworkManagerInterace {
    var resolver: Resolver?
    
    init(resolver: Resolver) {
        self.resolver = resolver
    }
    
    func getWeatherForCity(city: CityObject, callback: @escaping ((DataResult<CityWeatherInfoObject>) -> Void)) {
        let afnetworkManager = AFNetworkingManager()
        afnetworkManager.getWeatherInfo(city.cityName) { [weak self] weatherResult in
            if let result = weatherResult, let  cityWeatherObject = self?.bindWeatherInfoObjectToCityWeatherObject(weatherInfo: result){
                callback(DataResult.success(data: cityWeatherObject))
            }
        } withError:  { error in
            if let err = error {
                callback(DataResult.failure(error: err))
            }
        }
    }
    
    private func bindWeatherInfoObjectToCityWeatherObject(weatherInfo: WeatherInfoResult) -> CityWeatherInfoObject {
        let cityWeatherObject = CityWeatherInfoObject()
        cityWeatherObject.temperature = weatherInfo.temperature
        cityWeatherObject.requestDate = Date()
        cityWeatherObject.createdAt = cityWeatherObject.requestDate
        cityWeatherObject.humidty = weatherInfo.humidty
        cityWeatherObject.windSpeed = weatherInfo.windSpeed
        cityWeatherObject.weatherDescription = weatherInfo.weatherDescription
        cityWeatherObject.iconID = weatherInfo.weatherIcon
        return cityWeatherObject
    }
    
    
    
}
