//
//  CityWeatherPersistentManager.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 04/04/2022.
//

import Foundation
import Swinject
import MagicalRecord

protocol CityWeatherPersistentManagerInterface: PersistentStoreDataManagerInterface {
    func addWeatherInfo( weatherInfo: CityWeatherInfoObject, callBack: ((OperationResult) -> Void))
    func getWeatherInfoListWithCityID(cityID: Int64,callBack: ((DataCollectionResult<CityWeatherInfoObject>) -> Void))
    
}
// Here We are using Core Data:
class CityWeatherPersistentManager: CityWeatherPersistentManagerInterface {
    
    var resolver: Resolver?
    
    init(resolver: Resolver) {
        self.resolver = resolver
    }
    
    func addWeatherInfo(weatherInfo: CityWeatherInfoObject, callBack: ((OperationResult) -> Void)) {
        self.saveWeatherInfoObject(with: weatherInfo, callBack: callBack)
    }
    

    private func saveContext() {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
    }
    
    private func saveWeatherInfoObject(with weatherInfo: CityWeatherInfoObject, callBack: ((OperationResult) -> Void)) {
//        let SearchQueryId =  getMaxInsertedSearchQueryID()
        _ = self.addWeatherInfoToDB(weatherInfo: weatherInfo)
        callBack(OperationResult.success)
    }
    
     func getWeatherInfoList(callBack: ((DataCollectionResult<CityWeatherInfoObject>) -> Void)) {
        let weatherInfolist = self.fetchWeatherInfoList()
        callBack(DataCollectionResult.success(data: (weatherInfolist ?? [])))
    }
    func getWeatherInfoListWithCityID(cityID: Int64,callBack: ((DataCollectionResult<CityWeatherInfoObject>) -> Void)) {
       let weatherInfolist = self.fetchWeatherInfoWithCityID(cityID: cityID)
       callBack(DataCollectionResult.success(data: (weatherInfolist ?? [])))
   }
    
//
    // Get Max inserted  query Id
    private func getMaxInsertedObjectID() -> Int64 {
        var maxId = Int64(1)
        
        if let maxQueryID = WeatherInfoEntity.mr_aggregateOperation("max:", onAttribute: "weatherInfoID", with: nil) as? NSNumber {
            maxId = Int64(truncating: maxQueryID)
        }
        return maxId
    }
    
    // Clear all queries database
    private func clearWeatherInfoTable() {
        WeatherInfoEntity.mr_truncateAll()
    }
    
    
    // Bind query entity to query object
    private func bindWeatherInfoEntityToWeatherInfoObject(weatherEntity: WeatherInfoEntity) -> CityWeatherInfoObject {
        
        let weatherObject: CityWeatherInfoObject = CityWeatherInfoObject()
        
        weatherObject.temperature = weatherEntity.cityTemperature
        weatherObject.cityID = weatherEntity.cityID
        weatherObject.createdAt = weatherEntity.createdAt
        weatherObject.requestDate = weatherEntity.requestDate
        weatherObject.weatherDescription = weatherEntity.weatherDescription
        weatherObject.windSpeed  = weatherEntity.windSpeed
        weatherObject.humidty  = weatherEntity.humidity
        weatherObject.iconID  = weatherEntity.iconID
    
        return weatherObject
    }
    
    private func bindWeatherEntityListtoWeatherObjectList(infoList: [WeatherInfoEntity]) -> [CityWeatherInfoObject] {
         var weatherObjects: [CityWeatherInfoObject] = [CityWeatherInfoObject]()
         for info in infoList {
            let weatherObject = bindWeatherInfoEntityToWeatherInfoObject(weatherEntity: info)
             weatherObjects.append(weatherObject)
         }
        return weatherObjects
    }
    
    
    // Add query to database
    private func addWeatherInfoToDB(weatherInfo: CityWeatherInfoObject) -> WeatherInfoEntity? {
        let weatherEntity = WeatherInfoEntity.mr_createEntity()
        let weatherInfoID =  getMaxInsertedObjectID() + 1
        weatherEntity?.weatherInfoID = weatherInfoID
        weatherEntity?.cityTemperature =  weatherInfo.temperature ?? 0.0
        weatherEntity?.cityID = weatherInfo.cityID ?? 0
        weatherEntity?.createdAt = weatherInfo.createdAt
        weatherEntity?.requestDate = weatherInfo.requestDate
        weatherEntity?.weatherDescription = weatherInfo.weatherDescription
        weatherEntity?.windSpeed = weatherInfo.windSpeed ?? 0.0
        weatherEntity?.iconID = weatherInfo.iconID
        
        saveContext()
        
        return weatherEntity
    }
    
    // Get all queries objects
    private func fetchWeatherInfoList() -> [CityWeatherInfoObject]? {
        var weatherObjects =  [CityWeatherInfoObject]()
        if let weatherEnities: [WeatherInfoEntity] = WeatherInfoEntity.mr_findAllSorted(by: "requestDate", ascending: false) as? [WeatherInfoEntity] {
            weatherObjects = bindWeatherEntityListtoWeatherObjectList(infoList: weatherEnities)
        }
        return weatherObjects
    }
    
    // Get all queries objects based on query Id
     func fetchWeatherInfoWithCityID(cityID :Int64) -> [CityWeatherInfoObject]?  {
        var weatherObjects =  [CityWeatherInfoObject]()
        let predicate = NSPredicate(format: "cityID == %d", cityID)
        if let queryResult: [WeatherInfoEntity] = WeatherInfoEntity.mr_findAllSorted(by: "requestDate", ascending: false,with: predicate) as? [WeatherInfoEntity] {
            weatherObjects = bindWeatherEntityListtoWeatherObjectList(infoList: queryResult)
        }
        return weatherObjects
    }
    
}
