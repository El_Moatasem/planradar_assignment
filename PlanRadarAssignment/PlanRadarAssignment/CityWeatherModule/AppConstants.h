//
//  AppConstants.h
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 04/04/2022.
//

#ifndef AppConstants_h
#define AppConstants_h

static NSString * const weatherAPIsBaseUrl = @"https://openweathermap.org";
static NSString * const apisKey = @"f5cb0b965ea1564c50c6f1b74534d823";

static NSString * const weatherAPIsEndPoint = @"https://api.openweathermap.org/data/2.5/weather?q=%@&appid=%@";
static NSString * const weatherIconAPIsEndPoint = @"https://openweathermap.org/img/w/%@.png";



static NSString* const mainKey = @"main";
static NSString* const windKey = @"wind";
static NSString* const weatherKey = @"weather";
static NSString* const descriptionKey = @"description";
static NSString* const iconKey = @"icon";
static NSString* const speedKey = @"speed";
static NSString* const humidityKey = @"humidity";
static NSString* const tempKey = @"temp";

#endif /* AppConstants_h */
