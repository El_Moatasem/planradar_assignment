//
//  CityPreviewViewController.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 02/04/2022.
//

import Foundation
import UIKit
import Swinject
import SDWebImage

protocol CityWeatherViewInterface: ViewInterface {
    func setCityInfoObject(cityObject: CityObject)
    func setWeatherInfoObject(weatherInfo: CityWeatherInfoObject)
    func setWeatherInfoMode(mode: WeatherInfoMode)
}

class CityWeatherViewController: UIViewController {
  
    @IBOutlet weak var decriptionFieldLabel: UILabel!
    @IBOutlet weak var temperatureFieldLabel: UILabel!
    @IBOutlet weak var humitdyFieldLabel: UILabel!
    @IBOutlet weak var windspeedFieldLabel: UILabel!
    
    @IBOutlet weak var weatherDateTextLabel: UILabel!
    
    @IBOutlet weak var decriptionDataLabel: UILabel!
    @IBOutlet weak var temperatureDataLabel: UILabel!
    @IBOutlet weak var humidityDataLabel: UILabel!
    @IBOutlet weak var windSpeedDataLabel: UILabel!
    
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var viewTitleLabel: UILabel!
    var viewModel: CityViewModelInterface?
    var resolver: Resolver?
    @IBOutlet weak var weatherCardInfoView: UIView!
    @IBOutlet weak var cardContainerView: UIView!
    @IBOutlet weak var weatherInfoShadowView: UIView!
    
    class func createInstance(viewModel: CityViewModelInterface, resolver: Resolver) -> CityWeatherViewInterface {
        let viewController = UIStoryboard.init(name: CityWeatherUtilitiesUIConstants.cityWeatherStoryboardName, bundle: nil)
            .instantiateViewController(withIdentifier: CityWeatherUtilitiesUIConstants.cityWeatherControllerID) as! CityWeatherViewController
        viewController.viewModel = viewModel
        viewController.resolver = resolver
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpData()
        setUpUI()
    }
    
    private func setUpData() {
        
        if let cityObject  = self.viewModel?.getCityObject() {
            self.viewTitleLabel.text =  "\(cityObject.cityName?.uppercased() ?? "") \(cityObject.countryCode?.uppercased() ?? "")"
        }

        
        self.viewModel?.setDataReadyHandler { [weak self] in
            DispatchQueue.main.async {
                self?.hideLoading()
                if let weatherInfo = self?.viewModel?.getCurrentWeatherInfo(), let cityObject  = self?.viewModel?.getCityObject() {
                    if let icon = weatherInfo.iconID {
                        self?.weatherImageView.sd_setImage(with: URL(string: "\(ServerUtilities.weatherIconAPIsEndPoint)\(icon).png"), placeholderImage: nil)
                    }
                    self?.decriptionDataLabel.text = weatherInfo.weatherDescription ?? ""
                    
                    self?.temperatureDataLabel.text = "\(weatherInfo.temperature ?? 0.0) °C"
                    
                    self?.humidityDataLabel.text = "\(Int(weatherInfo.humidty ?? 0.0)) %"
                    
                    self?.windSpeedDataLabel.text = "\(weatherInfo.windSpeed ?? 0.0) km/h"
                    
                    self?.weatherDateTextLabel.text = "\(CityWeatherUtilitiesUIConstants.weatherDateTextFieldTextFirstPart) \(cityObject.cityName?.uppercased() ?? "") \(CityWeatherUtilitiesUIConstants.weatherDateTextFieldTextSecondPart) \(weatherInfo.requestDate?.toDateString(format: "dd.MM.yyyy - hh:mm") ?? "") "
                   
                }
            }
           
        }
        self.viewModel?.setErrorHandler { [weak self] (error)  in
            DispatchQueue.main.async {
                self?.hideLoading()
                self?.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
        
        
        let infoMode = self.viewModel?.getInfoMode()
        if infoMode == .requestMode {
            self.showLoadingView(message: "")
            self.viewModel?.getWeatherInfoRequest()
        } else {
            self.viewModel?.dispalyWeatherInfoData()
        }
        
        
        
    }
    
    private func setUpUI() {
        
        self.decriptionFieldLabel.text = CityWeatherUtilitiesUIConstants.descriptionFieldText
        self.temperatureFieldLabel.text = CityWeatherUtilitiesUIConstants.temperatureFieldText
        self.humitdyFieldLabel.text = CityWeatherUtilitiesUIConstants.humidityFieldText
        self.windspeedFieldLabel.text = CityWeatherUtilitiesUIConstants.windSpeedFieldText
        
        self.applyNavigationStyle()
        self.weatherCardInfoView.roundCorners(topLeft: 30, topRight: 50, bottomLeft: 50, bottomRight: 50)
        
        self.weatherInfoShadowView.layer.shadowPath = UIBezierPath(rect:  self.cardContainerView.bounds).cgPath
        self.weatherInfoShadowView.layer.shadowRadius = 15
        self.weatherInfoShadowView.layer.shadowColor = UIColor(netHex: 0x02010F,alpha: 0.2).cgColor
        self.weatherInfoShadowView.layer.shadowOffset = .zero
        self.weatherInfoShadowView.layer.shadowOpacity = 1
        self.weatherInfoShadowView.layer.cornerRadius = 15.0
        
    }
    
    
    @IBAction func dimissView(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    func applyNavigationStyle() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.largeTitleTextAttributes = textAttributes
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            appearance.titleTextAttributes = [.foregroundColor: UIColor.black]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.black]
            navigationItem.standardAppearance = appearance
            navigationItem.scrollEdgeAppearance = appearance
            navigationItem.compactAppearance = appearance
        } else {
            self.navigationController?.navigationBar.barTintColor = UIColor.black
        }
        
    }
    
}


extension CityWeatherViewController: CityWeatherViewInterface {
    var viewController: UIViewController {
        return self
    }
    
    var presentedView: UIView {
        return self.view
    }
    
    func setCityInfoObject(cityObject: CityObject) {
        self.viewModel?.setCityObject(city: cityObject)
    }
    func setWeatherInfoObject(weatherInfo: CityWeatherInfoObject) {
        self.viewModel?.setWeatherInfoObject(weatherInfo: weatherInfo)
        
    }
    func setWeatherInfoMode(mode: WeatherInfoMode) {
        self.viewModel?.setWeatherInfoMode(mode: mode)
    }
}


