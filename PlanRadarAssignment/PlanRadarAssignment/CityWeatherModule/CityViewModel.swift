//
//  CityViewModel.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 02/04/2022.
//

import Foundation
import Swinject

protocol CityViewModelInterface {
    func setCityObject(city: CityObject)
    func setWeatherInfoObject(weatherInfo: CityWeatherInfoObject)
    func setWeatherInfoMode(mode: WeatherInfoMode)
    func getCityObject() -> CityObject?
    func getCurrentWeatherInfo() -> CityWeatherInfoObject?
    func getWeatherInfoRequest()
    func dispalyWeatherInfoData()
    func setDataReadyHandler(handler: @escaping () -> Void)
    func setErrorHandler(handler: @escaping (Error) -> Void)
    func getInfoMode() -> WeatherInfoMode?
}

protocol WeatherListingViewModelInterface {
    func loadWeatherRequestsHistory()
    func getWeatherInfoListCount() -> Int?
    func getWeatherInfoItemAtIndex(index: Int) -> CityWeatherInfoObject?
    func setCityObject(city: CityObject)
    func getCityObject() -> CityObject?
    func setHistoryDataReadyHandler(handler: @escaping () -> Void)
    func setHistoryDataErrorHandler(handler: @escaping (Error) -> Void)
}



class CityViewModel: CityViewModelInterface {
    
    var resolver: Resolver?
    var networkManager: CityWeatherNetworkManagerInterace?
    var persistentManager: CityWeatherPersistentManagerInterface?
    
    
    // City Weather View
    private  var currentCityObject: CityObject?
    private var currentWeatherInfo: CityWeatherInfoObject?
    private var infoMode: WeatherInfoMode?
    
    private var dataIsReadyHandler: (() -> Void)?
    private var errorHandler: ((Error) -> Void)?
    

    
    // City History View
    var weatherInfoObjects: [CityWeatherInfoObject]?
    
    private var historyDataIsReadyHandler: (() -> Void)?
    private var historyDataErrorHandler: ((Error) -> Void)?
    
    
    init(resolver: Resolver, networkManager: CityWeatherNetworkManagerInterace, persistentManager: CityWeatherPersistentManagerInterface) {
        self.resolver = resolver
        self.networkManager = networkManager
        self.persistentManager = persistentManager
    }
    
    func setCityObject(city: CityObject) {
        self.currentCityObject = city
    }
    
    func getCityObject() -> CityObject? {
        return self.currentCityObject
    }
    
    func getInfoMode() -> WeatherInfoMode? {
        return infoMode
    }
    
    func getCurrentWeatherInfo() -> CityWeatherInfoObject? {
        return self.currentWeatherInfo
    }
    
    
    private func convertCalvinToCelcius(calvin: Double) -> Double {
        return (calvin - Double(273.15)).rounded()
    }
    
    func setWeatherInfoObject(weatherInfo: CityWeatherInfoObject) {
        self.currentWeatherInfo = weatherInfo
        self.dataIsReadyHandler?()
    }
    
    func setWeatherInfoMode(mode: WeatherInfoMode) {
        self.infoMode = mode
    }
    
    func dispalyWeatherInfoData() {
        self.dataIsReadyHandler?()
    }
    
   
    func getWeatherInfoRequest() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            if let currentCityObject = self?.currentCityObject {
                self?.networkManager?.getWeatherForCity(city: currentCityObject, callback: {[weak self] result in
                    if case let DataResult.success(data: cityWeatherInfo) = result {
                        self?.currentWeatherInfo = cityWeatherInfo
                        let temperature = self?.convertCalvinToCelcius(calvin: (cityWeatherInfo.temperature ?? 0))
                        self?.currentWeatherInfo?.temperature = temperature
                        self?.currentWeatherInfo?.cityID = self?.currentCityObject?.cityID
                        if let weatherInfo = self?.currentWeatherInfo {
                            self?.persistentManager?.addWeatherInfo(weatherInfo: weatherInfo, callBack: { result in
                            })
                        }
                        self?.dataIsReadyHandler?()
                    } else if case let DataResult.failure(error: error) = result {
                        self?.errorHandler?(error)
                    }
                })
            }
        }
    }
    

    func setErrorHandler(handler: @escaping (Error) -> Void) {
        self.errorHandler = handler
    }
    
    func setDataReadyHandler(handler: @escaping () -> Void) {
        self.dataIsReadyHandler = handler
    }
    
    
    
}

extension CityViewModel: WeatherListingViewModelInterface{
    func setHistoryDataErrorHandler(handler: @escaping (Error) -> Void) {
        self.historyDataErrorHandler = handler
    }

    
    func setHistoryDataReadyHandler(handler: @escaping () -> Void) {
        self.historyDataIsReadyHandler = handler
    }
    
    // Load from Database
    func loadWeatherRequestsHistory() {
        self.persistentManager?.getWeatherInfoListWithCityID(cityID: (self.currentCityObject?.cityID ?? 0),callBack: { [weak self] result in
            if case let DataCollectionResult.success(data: weatherInfoObjects) = result {
                self?.weatherInfoObjects = weatherInfoObjects
                self?.historyDataIsReadyHandler?()
            } else if case let DataCollectionResult.failure(error: error) = result {
                self?.historyDataErrorHandler?(error)
            }
        })
        
    }
    
    func getWeatherInfoListCount() -> Int? {
        return weatherInfoObjects?.count ?? 0
    }
    
    func getWeatherInfoItemAtIndex(index: Int) -> CityWeatherInfoObject? {
        return weatherInfoObjects?[index]
    }
    
}
