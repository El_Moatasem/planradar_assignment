//
//  CityWeatherBuilder.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 03/04/2022.
//

import Foundation
import Swinject

// Builder Design Pattern:
protocol CityWeatherBuilderInterface {
    func buildWeatherView() -> CityWeatherViewInterface
    func buildWeatherHistoryView() -> CityWeatherHistoryViewInterface
}

struct CityWeatherBuilder: CityWeatherBuilderInterface {
   
    let resolver: Resolver
    let viewModelInterface: CityViewModelInterface
    
    
    func buildWeatherView() -> CityWeatherViewInterface {
        let viewController = CityWeatherViewController.createInstance(viewModel: viewModelInterface, resolver: resolver)
        return viewController
    }
    
    func buildWeatherHistoryView() -> CityWeatherHistoryViewInterface {
        let viewController = CityWeatherHistoryController.createInstance(viewModel: viewModelInterface as! WeatherListingViewModelInterface, resolver: resolver)
        return viewController
    }
   

}

