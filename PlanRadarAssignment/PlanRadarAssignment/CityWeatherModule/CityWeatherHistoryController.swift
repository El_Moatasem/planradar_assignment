//
//  CityWeatherHistoryController.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 02/04/2022.
//

import Foundation
import UIKit
import Swinject

protocol CityWeatherHistoryViewInterface: ViewInterface {
    func setCityInfoObject(cityObject: CityObject)
}

class CityWeatherHistoryController: UIViewController {
  
    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var historyTableView: UITableView!
    var viewModel: WeatherListingViewModelInterface?
    var resolver: Resolver?
    
    class func createInstance(viewModel: WeatherListingViewModelInterface , resolver: Resolver) -> CityWeatherHistoryViewInterface {
        let viewController = UIStoryboard.init(name: CityWeatherUtilitiesUIConstants.cityWeatherStoryboardName, bundle: nil)
            .instantiateViewController(withIdentifier: CityWeatherUtilitiesUIConstants.cityWeatherHistoryViewID) as! CityWeatherHistoryController
        viewController.viewModel = viewModel
        viewController.resolver = resolver
        return viewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpData()
        setUpUI()
    }
    
    @IBAction func goBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setUpData() {
        self.viewModel?.loadWeatherRequestsHistory()
        
        // Reload TableView closure
        viewModel?.setHistoryDataReadyHandler { [weak self] in
            DispatchQueue.main.async {
                self?.historyTableView.reloadData()
            }
        }
        
        viewModel?.setHistoryDataErrorHandler { [weak self] (error)  in
            DispatchQueue.main.async {
                self?.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
        
        
        
    }
    
    private func setUpUI() {
        self.applyNavigationStyle()
        
        if let cityObject  = self.viewModel?.getCityObject() {
            self.viewTitleLabel.text =  "\(cityObject.cityName?.uppercased() ?? "") HISTORICAL"
        }
       
    }
    
    
    func applyNavigationStyle() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.largeTitleTextAttributes = textAttributes
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            appearance.titleTextAttributes = [.foregroundColor: UIColor.black]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.black]
            navigationItem.standardAppearance = appearance
            navigationItem.scrollEdgeAppearance = appearance
            navigationItem.compactAppearance = appearance
        } else {
            self.navigationController?.navigationBar.barTintColor = UIColor.black
        }
        
    }
    
}


extension CityWeatherHistoryController: CityWeatherHistoryViewInterface {
    var viewController: UIViewController {
        return self
    }
    
    var presentedView: UIView {
        return self.view
    }
    
    func setCityInfoObject(cityObject: CityObject) {
        self.viewModel?.setCityObject(city: cityObject)
        
    }
    
}


extension CityWeatherHistoryController: UITableViewDelegate {
    
    // MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72.0
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}

extension CityWeatherHistoryController: UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.getWeatherInfoListCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellCount = self.viewModel?.getWeatherInfoListCount() ?? 0
        if indexPath.row < cellCount {
            let cellIdentifier = CityWeatherUtilitiesUIConstants.weatherCellIdentifier
            let weatherCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? HistoryListingCell
            if let weatherInfo = self.viewModel?.getWeatherInfoItemAtIndex(index: indexPath.row), let cityObject = self.viewModel?.getCityObject() {
                weatherCell?.bind(cityObject: cityObject, weatherInfo: weatherInfo)
                weatherCell?.delegate = self
            }
            return weatherCell!
        }
        return UITableViewCell()
    }
    
}


extension CityWeatherHistoryController: HistoryListingCellDelegate {
    func didSelectWeatherInfo(weatherInfo: CityWeatherInfoObject,cityObject: CityObject) {
        let cityBuilder = resolver?.resolve(CityWeatherBuilder.self)
        let weatherView = cityBuilder?.buildWeatherView()
        weatherView?.setCityInfoObject(cityObject: cityObject)
        weatherView?.setWeatherInfoMode(mode: .displayInfoMode)
        weatherView?.setWeatherInfoObject(weatherInfo: weatherInfo)
        let navigationController = UINavigationController()
        navigationController.pushViewController(weatherView?.viewController ?? UIViewController(), animated: false)
        self.navigationController?.present(navigationController, animated: true)
    }
    
    
}
