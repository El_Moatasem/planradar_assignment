//
//  AFNetworkingManager.h
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 04/04/2022.
//

#ifndef AFNetworkingManager_h
#define AFNetworkingManager_h
#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "WeatherInfoResult.h"
#import "AppConstants.h"

typedef void(^WeatherCompletionHandler)(WeatherInfoResult* result);
typedef void(^WeatherErrorHandler)(NSError* error);


@interface AFNetworkingManager : NSObject
- (void)getWeatherInfo:(NSString *)cityName withCompletion: (WeatherCompletionHandler)completion withError:(WeatherErrorHandler)errorCompletion;





@end

#endif /* AFNetworkingManager_h */
