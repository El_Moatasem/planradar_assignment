//
//  AFNetworkingManager.m
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 04/04/2022.
//

#import <Foundation/Foundation.h>
#import "AFNetworkingManager.h"


@implementation AFNetworkingManager

- (void)getWeatherInfo:(NSString *)cityName withCompletion: (WeatherCompletionHandler)completion withError:(WeatherErrorHandler)errorCompletion;
{

    AFHTTPSessionManager* manager = [[AFHTTPSessionManager alloc]init];
    manager.requestSerializer = [[AFHTTPRequestSerializer alloc]init];
    
    NSString *url = [NSString stringWithFormat: weatherAPIsEndPoint, cityName,apisKey];
    NSString *encodedUrl = [url stringByAddingPercentEncodingWithAllowedCharacters :[NSCharacterSet URLQueryAllowedCharacterSet]];

    [manager GET:encodedUrl parameters:nil headers:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        WeatherInfoResult* result = [self parseResponseAndExtractData: responseObject];
        completion(result);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorCompletion(error);
    }];

}


-(WeatherInfoResult*) parseResponseAndExtractData:(id) responseObject {
    WeatherInfoResult* result = [[WeatherInfoResult alloc]init];

    NSDictionary* mainDict =  responseObject[mainKey];
    NSDictionary* windDict =  responseObject[windKey];
    NSArray* weatherList = responseObject[weatherKey];
    if ([weatherList count] > 0) {
        NSDictionary* weatherDict = weatherList[0];
        result.weatherDescription = weatherDict[mainKey];
        result.weatherIcon = weatherDict[iconKey];
    }

    result.windSpeed = [windDict[speedKey] doubleValue];
    result.humidty = [mainDict[humidityKey] doubleValue];
    result.temperature = [mainDict[tempKey] doubleValue];

    
    return result;
}
@end






