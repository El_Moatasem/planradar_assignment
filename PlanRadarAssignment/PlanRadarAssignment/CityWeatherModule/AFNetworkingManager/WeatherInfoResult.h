//
//  WeatherInfoResult.h
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 04/04/2022.
//

#ifndef WeatherInfoResult_h
#define WeatherInfoResult_h


@interface WeatherInfoResult: NSObject{
    
}
@property(nonatomic, strong) NSString* weatherDescription;
@property(nonatomic, strong) NSString* weatherIcon;
@property(nonatomic, assign) double temperature;
@property(nonatomic, assign) double humidty;
@property(nonatomic, assign) double windSpeed;



@end
#endif /* WeatherInfoResult_h */
