//
//  WeatherInfoResult.m
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 04/04/2022.
//

#import <Foundation/Foundation.h>
#import "WeatherInfoResult.h"


@implementation WeatherInfoResult

@synthesize weatherDescription;
@synthesize temperature;
@synthesize  humidty;
@synthesize windSpeed;
@synthesize weatherIcon;


@end


