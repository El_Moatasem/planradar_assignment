//
//  HistoryListingCell.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 04/04/2022.
//

import Foundation
import UIKit

protocol HistoryListingCellDelegate: AnyObject {
    func didSelectWeatherInfo(weatherInfo: CityWeatherInfoObject,cityObject: CityObject)
}



class HistoryListingCell: UITableViewCell {
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    private var weatherInfo: CityWeatherInfoObject?
    private var cityObject: CityObject?
    weak var delegate: HistoryListingCellDelegate?
    
    func bind(cityObject: CityObject, weatherInfo: CityWeatherInfoObject) {
        self.weatherInfo = weatherInfo
        self.cityObject = cityObject
        self.cityLabel.text = "\(cityObject.cityName?.uppercased() ?? "") , \(cityObject.countryCode?.uppercased() ?? "")"
        
        self.weatherLabel.text = "\(weatherInfo.weatherDescription ?? "") , \(weatherInfo.temperature ?? 0) °C"
   }
    @IBAction func didSelectWeatherInfo(_ sender: Any) {
        self.delegate?.didSelectWeatherInfo(weatherInfo: self.weatherInfo ?? CityWeatherInfoObject(),cityObject: self.cityObject ?? CityObject())
    }
    
}


