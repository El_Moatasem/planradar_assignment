//
//  AppAssembly.swift
//
//  Created by El Moatasem on 10/5/18.
//  Copyright © 2018 El Moatasem. All rights reserved.
//

import Foundation
import Swinject

/*
 Using to define dependcies among objects and Injecting them towards their containers
 */
struct AppAssembly: Assembly {
    
    func assemble(container: Container) {
        registerServices(in: container)
        registerModules(in: container)
    }
    
    private func registerServices(in container: Container) {
        
    }
    
    private func registerModules(in container: Container) {
        
        // Modules Configurations :
        registerCitiesListingFirstConfiguration (in: container)
        
        registerCityWeatherFirstConfiguration (in: container)
        
        
    }
    
    // ==================================================================
    // Cities Module Configurations :
    /*
     Injecting:
     Network Manager: CitiesListingNetworkManager (AFNetworking)
     Database Manager: Core-Data
     */
    
    private func registerCitiesListingFirstConfiguration (in container: Container) {

        container.register(CitiesListingNetworkManager.self, factory: { CitiesListingNetworkManager(resolver: $0)
        })
        
        
        container.register(CitiesListingPersistentManager.self, factory: { CitiesListingPersistentManager(resolver: $0)
        })

        container.register(CitiesViewModel.self, factory:{
            CitiesViewModel(resolver: $0, networkManager : CitiesListingNetworkManager(resolver: $0), persistentManager:  CitiesListingPersistentManager(resolver: $0))
        })
    
        container.register(CitiesListingBuilder.self, factory: {CitiesListingBuilder(resolver: $0, viewModelInterface:
                                                                                        CitiesViewModel(resolver: $0, networkManager : CitiesListingNetworkManager(resolver: $0), persistentManager: CitiesListingPersistentManager(resolver: $0)
            ))
        })
        
    }
    
    // Weather Info Module Configurations :
    /*
     Injecting:
     Network Manager: WeatherInfoListingNetworkManager (AFNetworking)
     Database Manager: Core-Data
     */
    
    private func registerCityWeatherFirstConfiguration (in container: Container) {

        container.register(CityWeatherNetworkManager.self, factory: { CityWeatherNetworkManager(resolver: $0)
        })
        
        
        container.register(CityWeatherPersistentManager.self, factory: { CityWeatherPersistentManager(resolver: $0)
        })

        container.register(CityViewModel.self, factory:{
            CityViewModel(resolver: $0, networkManager : CityWeatherNetworkManager(resolver: $0), persistentManager:  CityWeatherPersistentManager(resolver: $0))
        })
    
        container.register(CityWeatherBuilder.self, factory: {CityWeatherBuilder(resolver: $0, viewModelInterface:
                                                                                        CityViewModel(resolver: $0, networkManager : CityWeatherNetworkManager(resolver: $0), persistentManager: CityWeatherPersistentManager(resolver: $0)
            ))
        })
        
    }
    
    
    


}
