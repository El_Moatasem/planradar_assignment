//
//  ResponseDataParserInterface.swift
//  
//
//  Created by El Moatasem on 10/12/18.
//  Copyright © 2018 El Moatasem. All rights reserved.
//

import Foundation
/**
 Defines a common data parser interface to all datamanagers in modules.
 
 Expected to be overriden by another protocol specific to each view module.
 */
protocol ResponseDataParserInterface {
}

