//
//  ViewInterface.swift
//  
//
//  Created by El Moatasem on 10/5/18.
//  Copyright © 2018 El Moatasem. All rights reserved.
//

import UIKit

/**
 Defines a common interface to all view modules.
 
 Expected to be overriden by another protocol specific to each view module.
 */

protocol ViewInterface {
    var viewController: UIViewController { get }
    var presentedView: UIView  { get }
}

