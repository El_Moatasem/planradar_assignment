//
//  DataManagerInterface.swift
//  
//
//  Created by El Moatasem on 10/5/18.
//  Copyright © 2018 El Moatasem. All rights reserved.
//

import Foundation

/**
 Defines a common data manager interface to all datamanagers in modules.
 
 Expected to be overriden by another protocol specific to each view module.
 */
protocol DataManagerInterface {
}

/**
 Defines a common persistent store manager interface to all datamanagers in modules.
 
 Expected to be overriden by another protocol specific to each view module.
 */
protocol PersistentStoreDataManagerInterface: DataManagerInterface {
}

/**
 Defines a common server data manager interface to all datamanagers in modules.
 
 Expected to be overriden by another protocol specific to each view module.
 */
import Reachability
protocol NetworkDataManagerInterface: DataManagerInterface {
    func getConnectionStatus(callBack: @escaping ((Reachability.Connection) -> Void))
}


extension NetworkDataManagerInterface{
    func getConnectionStatus(callBack: @escaping ((Reachability.Connection) -> Void)) {
        ReachabilityHandler.shared.getConnectionStatus(callBack: callBack)
    }
}

