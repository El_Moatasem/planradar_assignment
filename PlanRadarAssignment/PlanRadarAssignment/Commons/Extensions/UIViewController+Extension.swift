//
//  UIViewController+Extension.swift
//  
//
//  Created by El Moatasem on 10/10/18.
//  Copyright © 2018 El Moatasem. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

extension UIViewController {
    func showToastMessage(message: String) {
        let hud =  MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = MBProgressHUDMode.text
        hud.detailsLabel.text = message
        hud.margin = 10
        hud.offset.y = 150
        hud.removeFromSuperViewOnHide = true
        hud.isUserInteractionEnabled = true
        hud.hide(animated: true, afterDelay: 3)
    }
    
    func showLoadingView(message: String) {
        let hud =  MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = message
    }
    
    func hideLoading() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func hideLoadingAfterDelay() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func addTapGestureForKeyBoardDismiss() {
        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
     func addLeftNavigationButton(_ view: UIView) {
         let leftItem:UIBarButtonItem = UIBarButtonItem()
         leftItem.customView = view
         self.navigationItem.leftBarButtonItem = leftItem
     }
     
      func addRightNavigationButton(_ view: UIView) {
         let rightItem:UIBarButtonItem = UIBarButtonItem()
         rightItem.customView = view
         self.navigationItem.rightBarButtonItem = rightItem
     }
    
    
}
