//
//  Date+Extension.swift
//  
//
//  Created by El Moatasem on 10/10/18.
//  Copyright © 2018 El Moatasem. All rights reserved.
//

import Foundation

extension Date {
    func toDateString(format: String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = format
        return dateformatter.string(from: self)
    }
}
