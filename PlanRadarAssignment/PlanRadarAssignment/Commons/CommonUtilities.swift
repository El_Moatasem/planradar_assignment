//
//  CommonUtilities.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 05/04/2022.
//

import Foundation
struct ServerUtilities {

    static let  weatherAPIsBaseUrl = "https://openweathermap.org";
    static let  apisKey = "f5cb0b965ea1564c50c6f1b74534d823";

    static let  weatherAPIsBaseEndPoint = "https://api.openweathermap.org/data/2.5/weather";
    static let  weatherIconAPIsEndPoint = "https://openweathermap.org/img/w/";
    
}

