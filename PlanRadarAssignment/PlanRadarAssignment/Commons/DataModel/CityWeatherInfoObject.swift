//
//  CityWeatherInfoObject.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 03/04/2022.
//

import Foundation

//Used as interface object between any model layer (persistent / network)
class CityWeatherInfoObject : NSObject, Codable {
    
    var cityID: Int64? = 0
    var weatherDescription: String?
    var temperature: Double?
    var humidty: Double?
    var windSpeed: Double?
    var requestDate: Date?
    var createdAt: Date?
    var iconID: String?
    
    
    enum CodingKeys: String, CodingKey {
        case cityID = "cityID"
        case weatherDescription = "weatherDescription"
        case temperature = "temperature"
        case humidty = "humidty"
        case windSpeed = "windSpeed"
        case requestDate = "requestDate"
        case createdAt = "createdAt"
        case iconID = "iconID"
    }
    
    
    override init(){
        
    }
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.cityID = try container.decode(Int64.self, forKey: .cityID)
        self.temperature = try container.decode(Double.self, forKey: .temperature)
        self.weatherDescription = try container.decode(String.self, forKey: .weatherDescription)
        self.humidty = try container.decode(Double.self, forKey: .humidty)
        self.windSpeed = try container.decode(Double.self, forKey: .windSpeed)
        self.requestDate = try container.decode(Date.self, forKey: .requestDate)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
        self.iconID = try container.decode(String.self, forKey: .iconID)
        
    }
    
}


