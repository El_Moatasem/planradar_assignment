//
//
//  Created by El Moatasem on 11/25/20.
//  Copyright © 2020 El Moatasem. All rights reserved.
//

import Foundation

//Used as interface object between any model layer (persistent / network)
class CityObject : NSObject, Codable {
    
    var cityID: Int64? = 0
    var cityName: String? = ""
    var cityDescription: String? = ""
    var countryCode: String?  = ""
    var createdAt: Date?
    
    
    enum CodingKeys: String, CodingKey {
        case cityID = "cityID"
        case cityName = "cityName"
        case countryCode = "countryCode"
        case cityDescription = "cityDescription"
        case createdAt = "createdAt"
    }
    

    
    override init() {
    }
    
    required init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.cityName = try container.decode(String.self, forKey: .cityName)
        self.cityDescription = try container.decode(String.self, forKey: .cityDescription)
        self.countryCode = try container.decode(String.self, forKey: .countryCode)
        self.cityID = try container.decode(Int64.self, forKey: .cityID)
        self.createdAt = try container.decode(Date.self, forKey: .createdAt)
    }
    
}

