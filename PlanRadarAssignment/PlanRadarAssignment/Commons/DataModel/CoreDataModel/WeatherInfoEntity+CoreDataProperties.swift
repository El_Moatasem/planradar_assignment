//
//  WeatherInfoEntity+CoreDataProperties.swift
//  
//
//  Created by El-Moatasem on 05/04/2022.
//
//

import Foundation
import CoreData


extension WeatherInfoEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeatherInfoEntity> {
        return NSFetchRequest<WeatherInfoEntity>(entityName: "WeatherInfoEntity")
    }

    @NSManaged public var cityID: Int64
    @NSManaged public var cityTemperature: Double
    @NSManaged public var createdAt: Date?
    @NSManaged public var humidity: Double
    @NSManaged public var requestDate: Date?
    @NSManaged public var weatherDescription: String?
    @NSManaged public var weatherInfoID: Int64
    @NSManaged public var windSpeed: Double
    @NSManaged public var iconID: String?

}
