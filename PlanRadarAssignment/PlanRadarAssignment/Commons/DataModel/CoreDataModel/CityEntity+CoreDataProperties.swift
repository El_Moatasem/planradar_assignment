//
//  CityEntity+CoreDataProperties.swift
//  
//
//  Created by El-Moatasem on 04/04/2022.
//
//

import Foundation
import CoreData


extension CityEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CityEntity> {
        return NSFetchRequest<CityEntity>(entityName: "CityEntity")
    }

    @NSManaged public var cityID: Int64
    @NSManaged public var cityName: String?
    @NSManaged public var countryCode: String?
    @NSManaged public var createdAt: Date?

}
