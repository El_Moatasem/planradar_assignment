//
//  ReachabilityHandler.swift
//  Pixabay
//
//  Created by El Moatasem on 11/30/20.
//  Copyright © 2020 El Moatasem. All rights reserved.
//

import Foundation
import Reachability

/*
 Handle Common Errors Like Internet Connection
 */
@objc protocol ReachabilityDelegate: AnyObject {
    @objc optional func didReceiveConnectionError(error: Error)
    @objc optional func didReceiveConnectionStateChange(message: String)
}

class ReachabilityHandler {
    
    private var reachability: Reachability?
    weak var delegate: ReachabilityDelegate?
    static let shared = ReachabilityHandler()
    
    struct InternetConnectionState {
        static let networkReachableViaWIFI = "Reachable via WIFI"
        static let networkReachableViaCellular = "Reachable via Cellular"
        static let internetNotFound = "Internet Not Found"
    }
    
    
     init() {
        reachability = try? Reachability()
        regsierAsInternetReachabilityListener()
    }
    
    func getConnectionStatus(callBack: @escaping ((Reachability.Connection) -> Void)) {
        reachability = try? Reachability()
        print("Reachability: ", (reachability?.connection ?? .wifi))
        callBack(reachability?.connection ?? .wifi)
    }
    
    
    func regsierAsInternetReachabilityListener() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability?.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    func unregsierAsInternetReachabilityListener(){
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        handleReachabilityState(reachability: reachability)
    }
    
    deinit {
        unregsierAsInternetReachabilityListener()
    }
    
    //    // MARK: - Handle Reachability State
         func handleReachabilityState(reachability: Reachability) {
            // Notify view with internet state
            switch reachability.connection {
            case .wifi:
                self.delegate?.didReceiveConnectionStateChange?(message: InternetConnectionState.networkReachableViaWIFI)
            case .cellular:
                self.delegate?.didReceiveConnectionStateChange?(message: InternetConnectionState.networkReachableViaCellular)
            case .none:
                self.delegate?.didReceiveConnectionError?(error: NSError(domain: ErrorDomains.serverError.rawValue, code: -1 , userInfo: [NSLocalizedDescriptionKey : InternetConnectionState.internetNotFound]))
            case .unavailable:
                self.delegate?.didReceiveConnectionError?(error: NSError(domain: ErrorDomains.serverError.rawValue, code: -1 , userInfo: [NSLocalizedDescriptionKey : InternetConnectionState.internetNotFound]))
            }
        }
}
