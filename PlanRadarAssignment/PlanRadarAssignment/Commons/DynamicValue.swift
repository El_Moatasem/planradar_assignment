//
//  DynamicValue.swift
//  MVVM_Project_Trial_1
//
//  Created by El-Moatasem on 20/02/2022.
//

import Foundation

class DynamicValue<T> {
    
    typealias CompletionHandler = (T) -> Void
    
    var value: T {
        didSet {
            self.notify()
        }
    }
    
    private var observers = [String: CompletionHandler]()
    
    init(_ value: T) {
        self.value = value
    }
    
    public func addObserver(_ observer: NSObject, completionHandler: @escaping CompletionHandler) {
        observers[observer.description] = completionHandler
    }
    
    
    public func addAndNotify(_ observer: NSObject, completionHandler: @escaping CompletionHandler) {
        observers[observer.description] = completionHandler
        self.notify()
    }
    
    private func notify() {
        observers.forEach({$0.value(value)})
    }
    
    
    deinit {
        observers.removeAll()
    }
    
}
