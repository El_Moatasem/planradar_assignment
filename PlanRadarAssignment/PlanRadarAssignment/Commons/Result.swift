import Foundation

/**
 Encapsulates the result of an operation. Possible cases to use that type:
 
 * An API request with no expected data to be returned. e.g. Deleting a resource
 * The result of form validation, with `.success` returned if the operation is successful and `.failure` is returned
 with a descriptive error of what failed validation.
 
 */
enum OperationResult {
    /** Operation completed successfully */
    case success
    /**
     Operation failed. Associated `Error` type (`error`)
     */
    case failure(error: Error)
}

/**
 Encapsulates the result of an operation that should return an object of `DataType` type. e.g. The result of a signin
 operation; if it succeeds, a user object is expected.
 */
enum DataResult<DataType> {
    case success(data: DataType)
    case failure(error: Error)
}

/**
 Encapsulates the result of an operation that should return a collection of objects of `DataType` type. e.g. The result
 of a search operation; if it succedes, a collection of objects is expected.
 */
enum DataCollectionResult<DataType> {
    case success(data: [DataType])
    case failure(error: Error)
}

/**
 Encapsulates the result of an operation that would return a page of a collection of `DataType`. The `.success` returns
 the current page and the count of total pages.
 */
enum PaginatedCollectionResult<DataType> {
    case success(data: [DataType], page: Int, totalPages: Int, totalListCount: Int)
    case failure(error: Error)
}
