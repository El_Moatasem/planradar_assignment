import Foundation


enum ErrorDomains: String {
    case serverError = "pixapay.error.serverError"
    case internetNotFound = "pixapay.error.internetNotFound"
    case requestTimeOut = "pixapay.error.requestTimeOut"
    
    var localizedDescription: String {
        get {
            switch self {
            case .requestTimeOut:
                return ErrorUtilities.requestTimeOutError
            case .serverError:
                return ErrorUtilities.internalServerError
            case .internetNotFound:
                return ErrorUtilities.internetNotFound
            }
        }
    }
}

class ErrorUtilities {
    
    static let requestTimeOutError = "Request Time Out"
    static let internalServerError = "Internal Server Error"
    static let internetNotFound = "Internet Not Found"
    
   static func getErrorCode(statusCode: Int) -> Int {
        switch statusCode {
        case 1,13,21:
            return 200
        case 2:
            return 501
        case 3,7,10,14,16,17,30,31,32,33,34:
            return 401
        case 4:
            return 405
        case 5,20:
            return 422
        case 6:
            return 404
        case 8:
            return 403
        case 9:
            return 503
        case 11,15:
            return 500
        case 12:
            return 201
        case 18,22,23,26,27,28,29:
            return 400
        case 19:
            return 406
        case 24:
            return 504
        case 25:
            return 429
        default:
            return statusCode
        }
    }
    
}

