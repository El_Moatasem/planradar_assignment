//
//
//  Created by El Moatasem on 10/5/18.
//  Copyright © 2018 El Moatasem. All rights reserved.
//

import Foundation
import UIKit

struct CityWeatherUtilities {
}

struct CityWeatherUtilitiesUIConstants {
   
    static let cityCellIdentifier = "cityCellId"
    static let citiesViewTitle = "Cities"
    static let cityWeatherStoryboardName = "CityWeatherView"
    static let cityWeatherControllerID = "cityWeatherControllerID"
    static let cityWeatherHistoryViewID = "cityWeatherHistoryViewID"
    static let weatherCellIdentifier = "weatherCellId"
    
    static let focusColor = UIColor.white
    static let unfocusColor = UIColor.gray
    
    static let descriptionFieldText = "Description".uppercased()
    
    static let temperatureFieldText = "Temperature".uppercased()
    static let humidityFieldText = "Humidity".uppercased()
    static let windSpeedFieldText = "Wind Speed".uppercased()
    
    
    static let weatherDateTextFieldTextFirstPart = "Weather information for".uppercased()
    static let weatherDateTextFieldTextSecondPart =  "received on \n".uppercased()
    
}





enum WeatherInfoMode {
    case requestMode
    case displayInfoMode
}
