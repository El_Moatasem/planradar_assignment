//
//  CreateAndEditFieldCell.swift
//  CombyneTVShow
//
//  Created by El-Moatasem on 25/09/2021.
//

import Foundation
import UIKit

class CreateAndEditFieldCell: UITableViewCell {
    
    @IBOutlet weak var entrySepView: UIView!
    @IBOutlet weak var entryTextField: UITextField!
    var mode: CreateAndEditCityMode?
    var currentCity: CityObject?
    private var fieldType: CreateAndEditFields?
    
    func bind(mode: CreateAndEditCityMode, fieldName: String, fieldType: CreateAndEditFields) {
        self.mode = mode
        self.entryTextField.placeholder = fieldName
        self.entryTextField.delegate = self
        self.fieldType = fieldType
        switch fieldType {
        case .cityName:
            self.entryTextField.text = self.currentCity?.cityName
            break
        case .countryCode:
            self.entryTextField.text = self.currentCity?.countryCode
            break
        default:
            break
        }
        self.entryTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        unfocusOnTextField(entryTextField)
        
    }
    
    func focusOnTextField(_ textField: UITextField) {
        entryTextField.setPlaceHolderColor(color: CitiesListingUtilitiesUIConstants.focusColor)
        entrySepView.backgroundColor = CitiesListingUtilitiesUIConstants.focusColor
    }


    func unfocusOnTextField(_ textField: UITextField) {
        entryTextField.setPlaceHolderColor(color: CitiesListingUtilitiesUIConstants.unfocusColor)
        entrySepView.backgroundColor = CitiesListingUtilitiesUIConstants.unfocusColor
    }

}

extension CreateAndEditFieldCell: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        focusOnTextField(textField)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        unfocusOnTextField(textField)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        switch self.fieldType {
        case .cityName:
            self.currentCity?.cityName = (textField.text ?? "")
            break
        case .countryCode:
            self.currentCity?.countryCode = (textField.text ?? "")
            break
        default:
            break
        }
    }
}


