//
//  CreateOrEditCityViewController.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 03/04/2022.
//

import Foundation
import UIKit
import Swinject

protocol CreateOrEditCityViewInterface: ViewInterface {
    
}
class CreateOrEditCityViewController: UIViewController {
  
    var viewModel: CitiesCreateOrEditViewModelInterface?
    var resolver: Resolver?
    @IBOutlet weak var createAndEditTableView: UITableView!
    
    class func createInstance(viewModel: CitiesCreateOrEditViewModelInterface, resolver: Resolver) -> CreateOrEditCityViewInterface {
        let viewController = UIStoryboard.init(name: CitiesListingUtilitiesUIConstants.citiesListingStoryboardName, bundle: nil)
            .instantiateViewController(withIdentifier: CitiesListingUtilitiesUIConstants.createOrEditViewControllerID) as! CreateOrEditCityViewController
        
        viewController.viewModel = viewModel
        viewController.resolver = resolver
        return viewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("CitiesListingViewController")
        setUpData()
        setUpUI()
    }
    
    @IBAction func goBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setUpData() {
        
    }
    
    private func setUpUI() {
        self.title = CitiesListingUtilitiesUIConstants.citiesViewTitle
    }
    
    func handleAddOrEditTVShow() {
       if self.viewModel?.getCreateAndEditCityMode() == .cityCreation {
           self.viewModel?.addCity()
           self.viewModel?.setCityAddedHandler {
               self.navigationController?.popViewController(animated: true)
           }
        }

       self.view.endEditing(true)
   }

    

}

extension CreateOrEditCityViewController: CreateOrEditCityViewInterface {
    var viewController: UIViewController {
        return self
    }
    
    var presentedView: UIView {
        return self.view
    }
}



extension CreateOrEditCityViewController: UITableViewDelegate {
    
    // MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

extension CreateOrEditCityViewController: UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel?.getCreateAndEditFieldListCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let fieldsCount = self.viewModel?.getCreateAndEditFieldListCount() ?? 0
        if indexPath.row < fieldsCount {
            let cellIdentifier = CitiesListingUtilitiesUIConstants.cityCreatAndEditCellIdentifier
            guard let fieldCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CreateAndEditFieldCell else {
                return UITableViewCell()
            }
            let currentMode = self.viewModel?.getCreateAndEditCityMode() ?? .cityCreation
            let currentCity = self.viewModel?.getCurrentCity()
            let fieldType = CreateAndEditFields.allCases[indexPath.row]
            fieldCell.currentCity = currentCity
            if let fieldName = self.viewModel?.getCreateAndEditFieldAtIndex(index: indexPath.row) {
                fieldCell.bind(mode: currentMode, fieldName: fieldName, fieldType: fieldType)
            }
            return fieldCell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        let cellIdentifier = CitiesListingUtilitiesUIConstants.createAndEditFooterCellIdentifier
        guard let footerCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CreateAndEditFooterCell else {
            return UIView()
        }
        let currentMode = self.viewModel?.getCreateAndEditCityMode() ?? .cityCreation
        if currentMode == .cityCreation {
            footerCell.bindCellData(mode: currentMode, buttonText: CitiesListingUtilitiesUIConstants.cityAddTitle)
        } else {
            footerCell.bindCellData(mode: currentMode, buttonText: CitiesListingUtilitiesUIConstants.cityEditTitle)
        }
        footerCell.delegate = self
        return footerCell

    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(80)
    }
//
}

extension CreateOrEditCityViewController: CreateAndEditFooterCellDelegate {
    func didPressAction(mode: CreateAndEditCityMode) {
        handleAddOrEditTVShow()
    }
}
