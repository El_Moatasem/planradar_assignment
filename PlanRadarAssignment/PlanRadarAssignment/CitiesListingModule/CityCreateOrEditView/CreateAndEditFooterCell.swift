//
//  CreateAndEditFooterCell.swift
//  CombyneTVShow
//
//  Created by El-Moatasem on 25/09/2021.
//

import Foundation
import UIKit


protocol CreateAndEditFooterCellDelegate: AnyObject {
    func didPressAction(mode: CreateAndEditCityMode)
}


class CreateAndEditFooterCell: UITableViewCell {
    
    @IBOutlet weak var addAndEditButton: UIButton!
    var mode: CreateAndEditCityMode?
    weak var delegate: CreateAndEditFooterCellDelegate?
    
    func bindCellData(mode: CreateAndEditCityMode, buttonText: String) {
        self.addAndEditButton.setTitle(buttonText, for: UIControl.State.normal)
        self.addAndEditButton.layer.cornerRadius = 10.0
        self.addAndEditButton.clipsToBounds = true
        self.mode = mode
    }
    @IBAction func addOrEditAction(_ sender: Any) {
        if let mode = self.mode {
            self.delegate?.didPressAction(mode: mode)
        }
        
    }
    
}
