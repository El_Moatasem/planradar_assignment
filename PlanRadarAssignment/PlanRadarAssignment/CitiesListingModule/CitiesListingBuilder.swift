//
//  CitiesListingBuilder.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 03/04/2022.
//

import Foundation
import Swinject

// Builder Design Pattern: 
protocol CitiesListingBuilderInterface {
    func buildCitiesListingView() -> CitiesListingViewInterface
    func buildCreateOrEditView() -> CreateOrEditCityViewInterface
}

struct CitiesListingBuilder: CitiesListingBuilderInterface {

    let resolver: Resolver
    let viewModelInterface: CitiesListingViewModelInterface
    
    func buildCitiesListingView() -> CitiesListingViewInterface {
        let viewController = CitiesListingViewController.createInstance(viewModel: viewModelInterface, resolver: resolver)
        return viewController
    }
    
    func buildCreateOrEditView() -> CreateOrEditCityViewInterface {
        let viewController = CreateOrEditCityViewController.createInstance(viewModel: viewModelInterface as! CitiesCreateOrEditViewModelInterface, resolver: resolver)
        return viewController
    }

}


