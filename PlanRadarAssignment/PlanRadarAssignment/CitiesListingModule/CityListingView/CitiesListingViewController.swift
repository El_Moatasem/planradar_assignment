//
//  CitiesListingModule.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 02/04/2022.
//

import Foundation
import UIKit
import Swinject

protocol CitiesListingViewInterface: ViewInterface {
    
}
class CitiesListingViewController: UIViewController {
  
    @IBOutlet weak var tableView: UITableView!
    var resolver: Resolver?
    
    var viewModel: CitiesListingViewModelInterface?
    class func createInstance(viewModel: CitiesListingViewModelInterface, resolver: Resolver) -> CitiesListingViewInterface {
        let viewController = UIStoryboard.init(name: CitiesListingUtilitiesUIConstants.citiesListingStoryboardName, bundle: nil)
            .instantiateViewController(withIdentifier: CitiesListingUtilitiesUIConstants.citiesListingViewControllerID) as! CitiesListingViewController
        
        viewController.viewModel = viewModel
        viewController.resolver = resolver
        return viewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpData()
        setUpUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.getCities()
    }
    
    private func setUpData() {
        
        viewModel?.getCities()
        // Reload TableView closure
        viewModel?.setDataReadyHandler { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        viewModel?.setErrorHandler { [weak self] (error)  in
            DispatchQueue.main.async {
                self?.showAlert(title: "Error", message: error.localizedDescription)
            }
        }
        
        viewModel?.setCityDeletedHandler { [weak self]  in
            self?.viewModel?.getCities()
        }
        
        
    }
    
    private func setUpUI() {
        self.title = CitiesListingUtilitiesUIConstants.citiesViewTitle
//        self.tableView.tableHeaderView  = self.tableHeaderView
        self.applyNavigationStyle()
    }
    
    func applyNavigationStyle() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.largeTitleTextAttributes = textAttributes
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            appearance.titleTextAttributes = [.foregroundColor: UIColor.black]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.black]
            navigationItem.standardAppearance = appearance
            navigationItem.scrollEdgeAppearance = appearance
            navigationItem.compactAppearance = appearance
        } else {
            self.navigationController?.navigationBar.barTintColor = UIColor.black
        }
        
    }
    @IBAction func addCityAction(_ sender: Any) {
        let citiesViewBuilder = resolver?.resolve(CitiesListingBuilder.self)
        let searchView = citiesViewBuilder?.buildCreateOrEditView()
        self.navigationController?.pushViewController(searchView?.viewController ?? UIViewController(), animated: true)
    }
    

}

extension CitiesListingViewController: CitiesListingViewInterface {
    var viewController: UIViewController {
        return self
    }
    
    var presentedView: UIView {
        return self.view
    }
}


extension CitiesListingViewController: UITableViewDelegate {
    
    // MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48.0
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let city = self.viewModel?.getCityAtIndex(index: indexPath.row) {
                self.viewModel?.deleteCity(cityObject: city)
            }
        }
    } 
    
}

extension CitiesListingViewController: UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.getCitiesListCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellCount = self.viewModel?.getCitiesListCount() ?? 0
        if indexPath.row < cellCount {
            let cellIdentifier = CitiesListingUtilitiesUIConstants.cityCellIdentifier
            let cityCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CityListingCell
            cityCell?.delegate = self
            if let city = self.viewModel?.getCityAtIndex(index: indexPath.row) {
                cityCell?.bind(cityObject: city)
            }
            return cityCell!
        }
        return UITableViewCell()
    }
    
}




extension CitiesListingViewController: CityListingCellDelegate {
    func showWeatherHistoryView(cityObject: CityObject) {
        let cityBuilder = resolver?.resolve(CityWeatherBuilder.self)
        let weatherView = cityBuilder?.buildWeatherHistoryView()
        weatherView?.setCityInfoObject(cityObject: cityObject)
        self.navigationController?.pushViewController(weatherView?.viewController ?? UIViewController(), animated: true)
       
    }
    
    func showWeatherInfoView(cityObject: CityObject) {
        let cityBuilder = resolver?.resolve(CityWeatherBuilder.self)
        let weatherView = cityBuilder?.buildWeatherView()
        weatherView?.setCityInfoObject(cityObject: cityObject)
        weatherView?.setWeatherInfoMode(mode: .requestMode)
        let navigationController = UINavigationController()
        navigationController.pushViewController(weatherView?.viewController ?? UIViewController(), animated: false)
        self.navigationController?.present(navigationController, animated: true)
    }
    
}
