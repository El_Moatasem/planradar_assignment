//
//  MovieDataCell.swift
//  CareemAssignment
//
//  Created by El Moatasem on 10/5/18.
//  Copyright © 2018 El Moatasem. All rights reserved.
//

import Foundation
import UIKit


protocol CityListingCellDelegate: AnyObject {
    func showWeatherHistoryView(cityObject: CityObject)
    func showWeatherInfoView(cityObject: CityObject)
}


class CityListingCell: UITableViewCell {
    @IBOutlet weak var cityLabel: UILabel!
    weak var delegate: CityListingCellDelegate?
    private var cityObject: CityObject?
    
    
    @IBAction func showWeatherHistoryView(_ sender: Any) {
        self.delegate?.showWeatherHistoryView(cityObject: cityObject ?? CityObject())
    }
    
    @IBAction func showWeatherInfoView(_ sender: Any) {
        self.delegate?.showWeatherInfoView(cityObject: cityObject ?? CityObject())
    }
    
    func bind(cityObject: CityObject) {
        self.cityObject = cityObject
        self.cityLabel.text = "\(cityObject.cityName ?? "") , \(cityObject.countryCode ?? "")"
   }
}



