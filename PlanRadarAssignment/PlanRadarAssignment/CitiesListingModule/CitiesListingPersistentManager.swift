//
//  CitiesPersistentManager.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 03/04/2022.
//

import Foundation
import Swinject
import MagicalRecord

protocol CitiesListingPersistentManagerInterface: PersistentStoreDataManagerInterface {
    func getCities(callBack: ((DataCollectionResult<CityObject>) -> Void))
    func deleteCity(city: CityObject, callBack: ((OperationResult) -> Void))
    func addCity(city: CityObject, callBack: ((OperationResult) -> Void))
    
}
// Here We are using Core Data: 
class CitiesListingPersistentManager: CitiesListingPersistentManagerInterface {
    
    var resolver: Resolver?
    
    init(resolver: Resolver) {
        self.resolver = resolver
    }
    
    func addCity(city: CityObject, callBack: ((OperationResult) -> Void)) {
        self.saveCityObject(with: city, callBack: callBack)
    }
    
    func deleteCity(city: CityObject, callBack: ((OperationResult) -> Void)) {
        let cityEntity = fetchSearchQueriesWithID(cityID: Int64(city.cityID ?? 0))
        cityEntity?.mr_deleteEntity()
        saveContext()
        callBack(OperationResult.success)
    }
    
    private func saveContext() {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
    }
    
    private func saveCityObject(with city: CityObject, callBack: ((OperationResult) -> Void)) {
//        let SearchQueryId =  getMaxInsertedSearchQueryID()
        _ = self.addCityToDB(cityObject: city)
        callBack(OperationResult.success)
    }
    
     func getCities(callBack: ((DataCollectionResult<CityObject>) -> Void)) {
        let cities = self.fetchCities()
        callBack(DataCollectionResult.success(data: (cities ?? [])))
    }
//
    // Get Max inserted  query Id
    private func getMaxInsertedSearchQueryID() -> Int64{
        var maxId = Int64(1)
        
        if let maxQueryID = CityEntity.mr_aggregateOperation("max:", onAttribute: "cityID", with: nil) as? NSNumber {
            maxId = Int64(truncating: maxQueryID)
        }
        return maxId
    }
    
    // Clear all queries database
    private func clearSearchQueriesDB() {
        CityEntity.mr_truncateAll()
    }
    
    
    // Bind query entity to query object
    private func bindCityEntityToCityObject(city: CityEntity) -> CityObject {
        let cityObject: CityObject = CityObject()
        cityObject.createdAt = city.createdAt
        cityObject.cityID = (city.cityID)
        cityObject.cityName = city.cityName
        cityObject.countryCode = city.countryCode
        return cityObject
    }
    
    private func bindCitiesEntityListtoCitiesObjectList(cities: [CityEntity]) -> [CityObject] {
         var citiesObjects: [CityObject] = [CityObject]()
         for city in cities {
            let cityObject = bindCityEntityToCityObject(city: city)
             citiesObjects.append(cityObject)
         }
        return citiesObjects
    }
    
    
    // Add query to database
    private func addCityToDB(cityObject: CityObject) -> CityEntity? {
        let cityEntity = CityEntity.mr_createEntity()
        let cityId =  getMaxInsertedSearchQueryID() + 1
        cityEntity?.cityID = cityId
        cityEntity?.createdAt = Date()
        cityEntity?.cityName = cityObject.cityName
        cityEntity?.countryCode = cityObject.countryCode
        saveContext()
        
        return cityEntity
    }
    
    // Get all queries objects
    private func fetchCities() -> [CityObject]? {
        var cityObjects =  [CityObject]()
        if let citiesEnities: [CityEntity] = CityEntity.mr_findAllSorted(by: "createdAt", ascending: false) as? [CityEntity] {
            cityObjects = bindCitiesEntityListtoCitiesObjectList(cities: citiesEnities)
        }
        return cityObjects
    }
    
    // Get all queries objects based on query Id
    private func fetchSearchQueriesWithID(cityID :Int64) -> CityEntity? {
        let predicate = NSPredicate(format: "cityID == %d", cityID)
        var cityEntity: CityEntity? = nil
        if let queryResult: [CityEntity] = CityEntity.mr_findAll(with: predicate) as? [CityEntity] {
            if(queryResult.count > 0) {
                cityEntity = queryResult[0]
            }
        }
       
        return cityEntity
    }
    
    
}

