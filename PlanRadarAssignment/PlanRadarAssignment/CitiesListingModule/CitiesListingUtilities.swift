//
//
//  Created by El Moatasem on 10/5/18.
//  Copyright © 2018 El Moatasem. All rights reserved.
//

import Foundation
import UIKit

struct CitiesListingUtilities {
    static let errorTitleMessage = "Error"
    static let infoTitleMessage = "Info"
    static let mediaType = "photo"
    static let defaultItemsPerPage = 200
    static let imageNotFound = "No Results"
}

struct CitiesListingUtilitiesUIConstants {
    static let loadingCellHeight = CGFloat(40)
    static let navTintColor = UIColor(red: 220, green: 20, blue: 60, alpha: 1)
    static let navTitleColor = UIColor.white
    static let searchBarTintColor = UIColor.black
    static let searchBarTextColor = UIColor.black
    static let searchBarBackgroundColor = UIColor.white
    static let searchBarPlaceHolderColor = UIColor.gray
    static let cityCellIdentifier = "cityCellId"
    static let citiesViewTitle = "Cities"
    static let citiesListingStoryboardName = "CitiesListingView"
    static let citiesListingViewControllerID = "citiesListingViewID"
    static let citiesListingNavControllerID = "citiesListingNavID"
    static let createOrEditViewControllerID = "createOrEditViewID"
    static let cityCreatAndEditCellIdentifier = "creatAndEditFieldCell"
    static let createAndEditFooterCellIdentifier  = "creatAndEditFooterCell"
    
    static let focusColor = UIColor.white
    static let unfocusColor = UIColor.gray
    
    
    
    static let cityAddTitle = "Add City"
    static let cityEditTitle = "Edit City"
    
}

enum CreateAndEditCityMode {
    case cityCreation
    case cityEditing
}


enum CreateAndEditFields: String, CaseIterable {
    case cityName = "Name"
    case countryCode = "Country Code"
}
