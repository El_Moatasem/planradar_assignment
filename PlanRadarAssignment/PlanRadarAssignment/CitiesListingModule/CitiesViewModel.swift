//
//  CitiesListingViewModel.swift
//  PlanRadarAssignment
//
//  Created by El-Moatasem on 02/04/2022.
//

import Foundation
import Swinject

protocol CitiesListingViewModelInterface {
    func getCities()
    func setDataReadyHandler(handler: @escaping () -> Void)
    func setErrorHandler(handler: @escaping (Error) -> Void)
    func getCitiesListCount() -> Int?
    func getCityAtIndex(index: Int) -> CityObject?
    func deleteCity(cityObject: CityObject) 
    func setCityDeletedHandler(handler: @escaping () -> Void)
}


protocol CitiesCreateOrEditViewModelInterface {
    func getCreateAndEditFieldListCount() -> Int
    func getCreateAndEditFieldAtIndex(index: Int) -> String?
    func getCreateAndEditCityMode() -> CreateAndEditCityMode
    func addCity()
    func getCurrentCity() -> CityObject?
    func setCityAddedHandler(handler: @escaping () -> Void)
}


class CitiesViewModel  {
    
    var resolver: Resolver?
    var networkManager: CitiesListingNetworkManagerInterface?
    var persistentManager: CitiesListingPersistentManagerInterface?
    
    // Listing Variables
    private var dataIsReadyHandler: (() -> Void)?
    private var errorHandler: ((Error) -> Void)?
    private var cityDeletedHandler: (() -> Void)?
    
    var cities: [CityObject]? {
        didSet{
            dataIsReadyHandler?()
        }
    }
    
    // Create Or Edit Variables
    var createAndEditFields: [String]?
    private var cityMode: CreateAndEditCityMode?
    private var currentCity: CityObject?
    private var cityAddedHandler: (() -> Void)?
    
    
    init(resolver: Resolver, networkManager: CitiesListingNetworkManagerInterface, persistentManager: CitiesListingPersistentManagerInterface) {
        self.resolver = resolver
        self.networkManager = networkManager
        self.persistentManager = persistentManager
        self.createAndEditFields = CreateAndEditFields.allCases.map({$0.rawValue})
        self.currentCity = CityObject()
    }
    
    
}

extension CitiesViewModel: CitiesCreateOrEditViewModelInterface{
    func addCity() {
        if let city = self.currentCity {
            self.persistentManager?.addCity(city: city, callBack: { [weak self] result in
                DispatchQueue.main.async {
                    self?.cityAddedHandler?()
                }
            })
        }
    }
    func setCityAddedHandler(handler: @escaping () -> Void) {
        self.cityAddedHandler = handler
    }
    
    func getCreateAndEditCityMode() -> CreateAndEditCityMode {
        return cityMode ?? .cityCreation
    }
    
    func getCurrentCity() -> CityObject? {
        return currentCity
    }
    
    func getCreateAndEditFieldAtIndex(index: Int) -> String? {
        return self.createAndEditFields?[index]
    }
    
    func getCreateAndEditFieldListCount() -> Int {
        return (self.createAndEditFields?.count ?? 0)
    }
    
}


extension CitiesViewModel: CitiesListingViewModelInterface{
    
    func deleteCity(cityObject: CityObject) {
        self.persistentManager?.deleteCity(city: cityObject, callBack: {[weak self] result in
            DispatchQueue.main.async {
                self?.cityDeletedHandler?()
            }
        })
    }
    
    func setCityDeletedHandler(handler: @escaping () -> Void) {
        self.cityDeletedHandler = handler
    }
    
    
    func setErrorHandler(handler: @escaping (Error) -> Void) {
        self.errorHandler = handler
    }
    
    func setDataReadyHandler(handler: @escaping () -> Void) {
        self.dataIsReadyHandler = handler
    }
    
    func getCities() {
        self.persistentManager?.getCities(callBack: { [weak self] result in
            if case let DataCollectionResult.success(data: cities) = result {
                self?.cities = cities
            } else if case let DataCollectionResult.failure(error: error) = result {
                self?.errorHandler?(error)
            }
        })
    }
    
    func getCitiesListCount() -> Int? {
        return self.cities?.count ?? 0
    }
    
    func getCityAtIndex(index: Int) -> CityObject? {
        return self.cities?[index]
    }
    
}
