# Plan Radar Assignment (Weather App)

I applied **MVVM** Architecture and **Dependency Injection** to inject the objects that are dependant on each other 

The Architecture follows **S.O.L.I.D** principles which are  :

* Single Responsibility Principle :   every module do only 1 job
* Open/Closed Principle : modules open for extension and closed for modification 
* Liskov Substitution Principle : All Types can be replaced by subtypes with no changes
* Interface Segregation Principle : No dependency on unused methods  
* Dependency Inversion : Details depend on abstraction and abstraction doesn't depend on details 


## Getting Started

Each Module has the following Components :

* View Model
* View
* Network Manager Interface
* Persistent Manager Interface

 

We have the following Interfaces :

* View Builder Interface
* View Interface
* View Model Interfaces
* Data Manager Interface
* Persistent Store Data Manager Interface
* Network Data Manager Interface



### Architecture Notes : 

* Persistent Store Data Manager Interface and Network Data Manager Interface inherits from  Data Manager Interface

* Data Manager Interface has the methods to model manipulation across all modules

* Network Data Manager Interface  has the methods for network calls and processing can be overridden by many concrete managers like Rest-API handling using Alamofire , GraphQL handling which make it flexible to change the network layer manager each time for specific module without changing anything 

* Persistent Store Data Manager Interface  has the methods for data persistence and data manipulation can be overridden by many concrete managers like Core-Data , Realm which make it flexible to change the persistence layer manager each time for specific module without changing anything 

* Error Handling : Errors are reported with code parsed as it has the following Format ([Error 400] "error message" ) parsed in all parsers and reported to the network manager  along with Internet Connection Error


### Librares/Frameworks/Drivers
I used the following Concrete libraries/IDEs : 

* Persistent Store Data Manager Interface : Core-Data(Magical-Record)  
* Network Data Manager Interface :AFNetworking 
* Dependency Injection : Swinject
* Image Caching : SDWebImage
* Loading library : MBProgressHUD as SVProgressHUD has problem with Scence Delegate (App crashes)
* Xcode : 13.3 (Latest Xcode)



### Configurations :
* Configurations for each module is handled in AppAssembly file  
* You can have many configurations for each module 
* You can changes the Network Manager or Database Manager or Response Parser for each module without changing the remaining project files
* Every Network Manager, Persistent Manager , Response Parser can be replaced easily without no change (Liskov Substitution Principle)
* Add your configurations on **registerModules** method : 

```swift
    private func registerModules(in container: Container) {
        
            // Modules Configurations :
        registerCitiesListingFirstConfiguration (in: container)
        
        registerCityWeatherFirstConfiguration (in: container)
        
    }

```
### Search Module Configurations :

Code Sample [1] :

* We Database Manager: Core-Data


```swift
         /*
     Injecting:
     Network Manager: CitiesListingNetworkManager (AFNetworking)
     Database Manager: Core-Data
     */
    
    private func registerCitiesListingFirstConfiguration (in container: Container) {

        container.register(CitiesListingNetworkManager.self, factory: { CitiesListingNetworkManager(resolver: $0)
        })
        
        
        container.register(CitiesListingPersistentManager.self, factory: { CitiesListingPersistentManager(resolver: $0)
        })

        container.register(CitiesViewModel.self, factory:{
            CitiesViewModel(resolver: $0, networkManager : CitiesListingNetworkManager(resolver: $0), persistentManager:  CitiesListingPersistentManager(resolver: $0))
        })
    
        container.register(CitiesListingBuilder.self, factory: {CitiesListingBuilder(resolver: $0, viewModelInterface:
                                                                                        CitiesViewModel(resolver: $0, networkManager : CitiesListingNetworkManager(resolver: $0), persistentManager: CitiesListingPersistentManager(resolver: $0)
            ))
        })
        
    }
```


Code Sample [2] :

* We used WeatherInfoListingNetworkManager (AFNetworking)
* We Database Manager: Core-Data

```swift
    /*
     Injecting:
     Network Manager: WeatherInfoListingNetworkManager (AFNetworking)
     Database Manager: Core-Data
     */
    
    private func registerCityWeatherFirstConfiguration (in container: Container) {

        container.register(CityWeatherNetworkManager.self, factory: { CityWeatherNetworkManager(resolver: $0)
        })
        
        
        container.register(CityWeatherPersistentManager.self, factory: { CityWeatherPersistentManager(resolver: $0)
        })

        container.register(CityViewModel.self, factory:{
            CityViewModel(resolver: $0, networkManager : CityWeatherNetworkManager(resolver: $0), persistentManager:  CityWeatherPersistentManager(resolver: $0))
        })
    
        container.register(CityWeatherBuilder.self, factory: {CityWeatherBuilder(resolver: $0, viewModelInterface:
                                                                                        CityViewModel(resolver: $0, networkManager : CityWeatherNetworkManager(resolver: $0), persistentManager: CityWeatherPersistentManager(resolver: $0)
            ))
        })
        
    }
```

### Challenges :

#### Objective-C Part :

* I created AFNetworking Mangager to get WeatherInfo in Objectiv-C and feedback to swift based network manager
* I created Objective Data Container to hold data from APIs from AFNetworking Objective-C Implmentation
* Error is Handled too for no data returned for invalid City (See Screenshots)

```Objective-C

@implementation AFNetworkingManager

- (void)getWeatherInfo:(NSString *)cityName withCompletion: (WeatherCompletionHandler)completion withError:(WeatherErrorHandler)errorCompletion;
{

    AFHTTPSessionManager* manager = [[AFHTTPSessionManager alloc]init];
    manager.requestSerializer = [[AFHTTPRequestSerializer alloc]init];
    
    NSString *url = [NSString stringWithFormat: weatherAPIsEndPoint, cityName,apisKey];
    NSString *encodedUrl = [url stringByAddingPercentEncodingWithAllowedCharacters :[NSCharacterSet URLQueryAllowedCharacterSet]];

    [manager GET:encodedUrl parameters:nil headers:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        WeatherInfoResult* result = [self parseResponseAndExtractData: responseObject];
        completion(result);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorCompletion(error);
    }];

}


-(WeatherInfoResult*) parseResponseAndExtractData:(id) responseObject {
    WeatherInfoResult* result = [[WeatherInfoResult alloc]init];

    NSDictionary* mainDict =  responseObject[mainKey];
    NSDictionary* windDict =  responseObject[windKey];
    NSArray* weatherList = responseObject[weatherKey];
    if ([weatherList count] > 0) {
        NSDictionary* weatherDict = weatherList[0];
        result.weatherDescription = weatherDict[mainKey];
        result.weatherIcon = weatherDict[iconKey];
    }

    result.windSpeed = [windDict[speedKey] doubleValue];
    result.humidty = [mainDict[humidityKey] doubleValue];
    result.temperature = [mainDict[tempKey] doubleValue];

    
    return result;
}
@end

```


#### Cities Module :

* I have *CitiesViewController* and *CreateOrEditCityViewController* on same module to follow the single responsibility principle as each controller do only 1 job and each has it's own ViewMdodel Interface


```swift
  protocol CitiesListingViewModelInterface {
    func getCities()
    func setDataReadyHandler(handler: @escaping () -> Void)
    func setErrorHandler(handler: @escaping (Error) -> Void)
    func getCitiesListCount() -> Int?
    func getCityAtIndex(index: Int) -> CityObject?
    func deleteCity(cityObject: CityObject) 
    func setCityDeletedHandler(handler: @escaping () -> Void)
}


protocol CitiesCreateOrEditViewModelInterface {
    func getCreateAndEditFieldListCount() -> Int
    func getCreateAndEditFieldAtIndex(index: Int) -> String?
    func getCreateAndEditCityMode() -> CreateAndEditCityMode
    func addCity()
    func getCurrentCity() -> CityObject?
    func setCityAddedHandler(handler: @escaping () -> Void)
}

```

#### Weather Info Module :

* I have *CityWeatherViewController* and *CityWeatherHistoryController* on same module to follow the single responsibility principle as each controller do only 1 job and each has it's own ViewMdodel Interface


```swift
protocol CityViewModelInterface {
    func setCityObject(city: CityObject)
    func setWeatherInfoObject(weatherInfo: CityWeatherInfoObject)
    func setWeatherInfoMode(mode: WeatherInfoMode)
    func getCityObject() -> CityObject?
    func getCurrentWeatherInfo() -> CityWeatherInfoObject?
    func getWeatherInfoRequest()
    func dispalyWeatherInfoData()
    func setDataReadyHandler(handler: @escaping () -> Void)
    func setErrorHandler(handler: @escaping (Error) -> Void)
    func getInfoMode() -> WeatherInfoMode?
}

protocol WeatherListingViewModelInterface {
    func loadWeatherRequestsHistory()
    func getWeatherInfoListCount() -> Int?
    func getWeatherInfoItemAtIndex(index: Int) -> CityWeatherInfoObject?
    func setCityObject(city: CityObject)
    func getCityObject() -> CityObject?
    func setHistoryDataReadyHandler(handler: @escaping () -> Void)
    func setHistoryDataErrorHandler(handler: @escaping (Error) -> Void)
}



```
